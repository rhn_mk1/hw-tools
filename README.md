Hardware debugging tools
========================

A collection of tools useful for hardware debugging.

`buspirate_spi.py`
------------------

Uses the serial connection over `/dev/ttyUSB0` to Bus Pirate v3 to read values provided over SPI. Slave mode.

Uses the CS, CLK, MOSI, MISO pins. Reads 4 bytes at a time to form little-endian words on both MOSI and MISO registers.
